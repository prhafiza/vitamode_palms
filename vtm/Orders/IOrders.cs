﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace vtm.Orders
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IOrders" in both code and config file together.
    [ServiceContract]
    public interface IOrders
    {
       
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "RequestNewOrder")]
        Stream RequestNewOrder(Stream streamdata);      

    }
}
