﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using vtm.Common;
using vtm.Customers;

namespace vtm.Orders
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Orders" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Orders.svc or Orders.svc.cs at the Solution Explorer and start debugging.
    public class Orders : IOrders
    {
        clsOrders clsOrders = new clsOrders();
        clsCommon clsCommon = new clsCommon();
        clsCustomers clsCustomers = new clsCustomers();
             
        #region  "RequestNewOrder"
        public Stream RequestNewOrder(Stream streamdata)       
        {
            StreamReader reader = new StreamReader(streamdata);
            string res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();
            var @params = HttpUtility.ParseQueryString(res);
            JavaScriptSerializer ser3 = new JavaScriptSerializer();
            dynamic array = new ExpandoObject();
            dynamic array2 = new ExpandoObject();
            array = JsonConvert.DeserializeObject(res);
            string jsonread = "";
            string json;

            dynamic ObjRegCus = new ExpandoObject(); // NEW REG
            dynamic objUDF = new ExpandoObject();
            dynamic ObjJobID = new ExpandoObject();
            dynamic ObjJobMaster = new ExpandoObject();
            dynamic ObjConsignor = new ExpandoObject();
            dynamic ObjConsignee = new ExpandoObject();
            dynamic ObjOriClientAddRefID = new ExpandoObject(); // BILLING SENDER
            dynamic ObjDestClientAddRefID = new ExpandoObject(); // BILLING CLIENT
            dynamic ObjAddRefID = new ExpandoObject();
            dynamic ObjDocNo = new ExpandoObject();
            dynamic ObjJobDoc = new ExpandoObject();
            dynamic ObjJobItem = new ExpandoObject();
            dynamic ObjJobDetail = new ExpandoObject();
            dynamic ObjCtry = new ExpandoObject();
            dynamic ObjOrderedProduct = new ExpandoObject();
            dynamic ObjRegCusdt = new ExpandoObject();
            dynamic ObjState = new ExpandoObject();
            dynamic ObjAddrs = new ExpandoObject();
            dynamic ObjWhsCode = new ExpandoObject();
            dynamic ObjOrderInfo = new ExpandoObject();
            dynamic ObjBillType = new ExpandoObject();
            dynamic ObjUpdateIsTax = new ExpandoObject();
            dynamic insertnewAddr = new ExpandoObject();

           // using (StreamReader r = new StreamReader(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/file/bill.json"))
            //using (StreamReader r = new StreamReader(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/file/billandship.json"))
            //{
            //    jsonread = r.ReadToEnd();
            //    JavaScriptSerializer ser = new JavaScriptSerializer();
            //    array = JsonConvert.DeserializeObject(jsonread);
            //}

            int intCheckTaxFree =  clsOrders.checkTaxFree(array); // 0 = non tax free ; 1 = tax free // IsTax = 1;
           

            string customerid = array.customer_id.ToString();
            string email = array.customerinfo.billing.email.ToString();

            if (customerid.Equals("0"))
            {
                ObjConsignee = clsOrders.getConsigneeClientRefID(array.customerinfo.billing.email.ToString());
            }
            else
            {
                ObjConsignee = clsOrders.getConsigneeClientRefID(customerid);
            }

            #region state
            int StateRefID = 0;
            dynamic findState = new ExpandoObject();
           
            findState = clsOrders.findStateJson(array.customerinfo.billing.state.ToString());
            ObjState = clsCustomers.getStateRefID(findState.statename);
            if (ObjState != null)
            {
                if (ObjState.rowID > 0) { StateRefID = ObjState.rowID; }
            }                       

            #region  array state
            //if (mapState.ContainsKey(array.customerinfo.billing.state.ToString()))
            //{
            //    if (!mapState[array.customerinfo.billing.state.ToString()].Trim().Equals(""))
            //    {
            //        ObjState = clsCustomers.getStateRefID(mapState[array.customerinfo.billing.state.ToString()]);
            //        if (ObjState != null)
            //        {
            //            if (ObjState.rowID > 0) { StateRefID = ObjState.rowID; }
            //        }
            //    }
            //}
            #endregion
            #endregion

            #region orderid
            ObjOrderInfo.orderid = array.order.id.ToString();
            //check if order id exists, abort
            ObjOrderInfo.checkexists = clsOrders.CheckOrderExists(array.order.id.ToString());
            if (ObjOrderInfo.checkexists.rowCount == 0)
            {
                ObjOrderInfo.Status = "Not Exists";
                #endregion

                #region check customer
                #region guest
                if (ObjConsignee.rowID == 0) //reg newcustomer
                {
                    if (customerid.Equals("0"))
                    {
                        ObjRegCusdt.userid = array.customerinfo.billing.email.ToString();
                    }
                    else
                    {
                        ObjRegCusdt.userid = customerid;
                    }

                    ObjRegCusdt.name = array.customerinfo.billing.first_name.ToString() + " , " + array.customerinfo.billing.last_name.ToString();
                    ObjRegCusdt.user_email = array.customerinfo.billing.email.ToString();
                    ObjRegCusdt.role = "EC";
                    ObjRegCus = clsOrders.RegisterNewCustomer(ObjRegCusdt , intCheckTaxFree);

                    ObjRegCus.newID = Convert.ToInt32(ObjRegCus.newID);
                    if (ObjRegCus.newID > 0)
                    {                        
                        ObjRegCus.rowID = ObjRegCus.newID;
                        objUDF = clsCustomers.InsertClientUDF(ObjRegCus.newID);
                        ObjAddrs = clsOrders.InsertShippingAddress(array, ObjRegCus, StateRefID);
                        // insertnewAddr = clsOrders.InsertBillingAddress(array, ObjRegCus, StateRefID);
                        //ObjAddrs = clsOrders.getUserBillingAddressRefID(array, ObjRegCus, StateRefID);
                    }
                }
                #endregion
                #region registered customer
                else
                {
                    // update istax
                    ObjUpdateIsTax = clsOrders.UpdateCustomer(ObjConsignee , intCheckTaxFree);
                    // update billing address

                    ObjAddrs = clsOrders.InsertShippingAddress(array, ObjConsignee, StateRefID);
                    //insertnewAddr = clsOrders.UpdateBillingAddress(array, ObjConsignee, StateRefID);
                    //ObjAddrs = clsOrders.getUserBillingAddressRefID(array, ObjConsignee, StateRefID);
                }
                #endregion
                #endregion
                // check address
                // if not exist add
                //ObjState = clsOrders.getStateRefID(FormJobID.StateCode);
                var ListJobItem = new List<dynamic>();
                var ListJobDetails = new List<dynamic>();
                #region processing
                dynamic FormJobID = new ExpandoObject(); FormJobID.StateCode = array.customerinfo.billing.state.ToString();


                ObjJobID = clsOrders.GetAndSetJobID(FormJobID);
                if (ObjJobID != null)
                {
                    ObjConsignor = clsOrders.getConsignorClientRefID(ObjJobID);
                    if (ObjConsignor.rowID > 0)
                    {
                        ObjJobID.ConsignorClientRefID = ObjConsignor.rowID;
                        if (ObjConsignee.rowID == 0)
                        {
                            ObjJobID.ConsigneeClientRefID = ObjRegCus.newID;
                        }
                        else
                        {
                            ObjJobID.ConsigneeClientRefID = ObjConsignee.rowID;
                        }

                        ObjAddRefID = clsOrders.getOriClientAddRefID(ObjJobID);
                        ObjJobID.OriClientAddRefID = ObjAddRefID.OriClientAddRefID;
                        ObjJobID.DestClientAddRefID = ObjAddrs.DestClientAddRefID;
                        // ObjJobID.DestClientAddRefID = ObjAddRefID.DestClientAddRefID;

                        ObjJobMaster = clsOrders.InsertJobMaster(ObjJobID);
                        // ObjDocNo = clsOrders.GetAndSetDocNo(ObjJobID.WhsRefID, ObjJobID.ObjWarehouse.WhsCode);
                        ObjDocNo.DocNo = "EC" + ObjOrderInfo.orderid;
                        ObjJobDoc = clsOrders.InsertJobDocument(ObjJobMaster.newID, ObjDocNo.DocNo);

                        //testing                    
                        ObjOrderedProduct = clsOrders.OrderedProduct(array.product);

                        ObjCtry = clsOrders.getCtryRefID(array.customerinfo.shipping.country.ToString());
                        //  ObjCtry = clsOrders.getCtryRefID("MYR");
                        dynamic dataJobItem = new ExpandoObject();
                        dataJobItem.DocRefID = ObjJobDoc.newID;
                        dynamic dyJobItem = new ExpandoObject();
                        dynamic dyJobDetail = new ExpandoObject();
                        foreach (dynamic loop in ObjOrderedProduct.data)
                        {
                            foreach (dynamic iloop in loop)
                            {
                                if (iloop.Key.Equals("qty"))
                                {
                                    dataJobItem.ExpcQty = iloop.Value.ToString();
                                }
                                else if (iloop.Key.Equals("ItemRefID"))
                                {
                                    dataJobItem.ItemRefID = iloop.Value.ToString();
                                }
                                else if (iloop.Key.Equals("ItemClientRefID"))
                                {
                                    dataJobItem.ItemClientRefID = iloop.Value.ToString();
                                }
                                else if (iloop.Key.Equals("price"))
                                {
                                    dataJobItem.UnitPrice = iloop.Value.ToString();
                                }

                                dataJobItem.CtryRefID = ObjCtry.CtryRefID;
                            }

                            ObjBillType = clsOrders.GetBillType();
                            dataJobItem.billtype = ObjBillType.rowid;
                            dyJobItem = clsOrders.InsertJobItem(dataJobItem);
                            ListJobItem.Add(dyJobItem);
                        }
                        ObjJobItem = ListJobItem;
                        // dynamic objdt = clsOrders.InsertJobDetail(ObjJobMaster.newID);
                        ObjJobDetail = clsOrders.InsertJobDetail(ObjJobMaster.newID, array);

                        // ListJobDetails.Add(objdt);

                    }
                    //ObjJobDetail = ListJobDetails;
                }
                #endregion

                var DyObjectsList = new List<dynamic>();
                dynamic DyObj = new ExpandoObject();
                json = Newtonsoft.Json.JsonConvert.SerializeObject(new
                {
                    //objReg = array,
                    ObjWhsCode = ObjWhsCode,
                    ObjAddrs = ObjAddrs,
                    ObjOrderedProduct = ObjOrderedProduct,
                    billState = array.customerinfo.billing.state,
                    orderid = array.order.id,
                    ObjConsignee = ObjConsignee,
                    customerid = array.customerinfo.customer_id,
                    ObjRegCus = ObjRegCus,
                    GetAndSetJobID = ObjJobID,
                    ObjConsignor = ObjConsignor,
                    ObjJobMaster = ObjJobMaster,
                    ObjDocNo = ObjDocNo,
                    ObjAddRefID = ObjAddRefID,
                    ObjJobDoc = ObjJobDoc,
                    ObjJobItem = ObjJobItem,
                    ObjJobDetail = ObjJobDetail,
                    ObjCtry = ObjCtry,
                    ObjUpdateIsTax = ObjUpdateIsTax

                });
            }
            else
            {
                ObjOrderInfo.Status = "Exists";
                json = Newtonsoft.Json.JsonConvert.SerializeObject(new
                {
                    ObjOrderInfo = ObjOrderInfo
                });
            }
            return new MemoryStream(Encoding.UTF8.GetBytes(clsCommon.aaSerializeJsona(json)));
        }
        #endregion
        

    }
}
