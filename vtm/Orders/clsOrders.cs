﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace vtm.Orders
{
    public class clsOrders
    {
        #region "variable"
        string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();       
        #endregion

        #region '-- InsertJobMaster --'
        public dynamic InsertJobMaster(dynamic a)
        {
            dynamic rst = new ExpandoObject();
            string retval;
            int newID = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "INSERT INTO dbo.tbl_OrderJobMaster (JobID ,  WhsRefID , ConsignorClientRefID , ConsigneeClientRefID , OriClientAddRefID , DestClientAddRefID , "
                    + "ETA , ETD , JobStatus , IsSup , IsBond , IsUrgent)" +
                    " VALUES (@JobID , @WhsRefID , @ConsignorClientRefID , @ConsigneeClientRefID , @OriClientAddRefID , @DestClientAddRefID ,"
                + "GETDATE() , GETDATE() , @JobStatus , @IsSup , @IsBond , @IsUrgent); SELECT SCOPE_IDENTITY();";

                a.JobStatus = "OTOT";
                a.IsSup = 0;
                a.IsBond = 0;
                a.IsUrgent = 0;

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@JobID", a.JobID);
                command.Parameters.AddWithValue("@WhsRefID", a.WhsRefID);
                command.Parameters.AddWithValue("@ConsignorClientRefID", a.ConsignorClientRefID);
                command.Parameters.AddWithValue("@ConsigneeClientRefID", a.ConsigneeClientRefID);
                command.Parameters.AddWithValue("@OriClientAddRefID", a.OriClientAddRefID);
                command.Parameters.AddWithValue("@DestClientAddRefID", a.DestClientAddRefID);
                command.Parameters.AddWithValue("@JobStatus", a.JobStatus);
                command.Parameters.AddWithValue("@IsSup", a.IsSup);
                command.Parameters.AddWithValue("@IsBond", a.IsBond);
                command.Parameters.AddWithValue("@IsUrgent", a.IsUrgent);

                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Inserted ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.newID = newID;
                rst.retval = retval;
                return rst;
            }
        }
        #endregion

        #region '-- InsertJobDocument --'
        public dynamic InsertJobDocument(int JobRefID, string DocNo)
        {
            dynamic rst = new ExpandoObject();
            string retval;
            int newID = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "INSERT INTO dbo.tbl_OrderJobDocument (JobRefID , DocNo , DocType , UpdBy , UpdOn)" +
                    " VALUES (@JobRefID , @DocNo , @DocType , @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@JobRefID", JobRefID);
                command.Parameters.AddWithValue("@DocNo", DocNo);
                command.Parameters.AddWithValue("@DocType", "EC");
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Inserted ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.newID = newID;
                rst.retval = retval;
                return rst;
            }
        }
        #endregion

        #region '-- GetAndSetDocNo --'
        public dynamic GetAndSetDocNo(int WhsRefID, string whscode)
        {
            DataTable dt = new DataTable();
            dynamic rst = new ExpandoObject();
            dynamic ObjWarehouse = new ExpandoObject();
            dynamic ObjJob = new ExpandoObject();
            string retval;
            int newID = 0;
            int rowsCount = 0;
            //a.StateCode = "PNG";

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM dbo.tbl_PalmsysPrefix WHERE WhsRefID = @WhsRefID AND PrefixID = @PrefixID";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@WhsRefID", WhsRefID);
                command.Parameters.AddWithValue("@PrefixID", "EC");
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    rowsCount = sda.Fill(dt);
                    retval = "Successfully Selected ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.newID = newID;
                rst.retval = retval;
                rst.rowCount = rowsCount;
                rst.WhsRefID = WhsRefID;

                if (dt.Rows.Count == 1)
                {
                    rst.NewSerialNo = (int)dt.Rows[0]["SerialNo"] + 1;
                    rst.SerialNo = dt.Rows[0]["SerialNo"].ToString();
                    rst.PrefixID = dt.Rows[0]["PrefixID"].ToString();
                    rst.SerialDigit = dt.Rows[0]["SerialDigit"].ToString();
                    rst.StringSerialNo = Convert.ToString(rst.NewSerialNo).PadLeft(int.Parse(rst.SerialDigit), '0');
                    rst.DocNo = whscode+""+rst.PrefixID + "" + rst.WhsRefID + "" + rst.StringSerialNo;
                    ObjJob = updatePrefixDocNo(rst);
                    rst.UpdateJob = ObjJob;
                }

            }

            return rst;
        }

        #endregion

        #region '-- GetWhsCode --'
        public dynamic GetWhsCode(int WhsRefID)
        {
            DataTable dt = new DataTable();
            dynamic rst = new ExpandoObject();
            dynamic ObjWarehouse = new ExpandoObject();
            dynamic ObjJob = new ExpandoObject();
            string retval;
            int newID = 0;
            int rowsCount = 0;
            rst.WhsCode = 0;


            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM dbo.tbl_SetupWareHouse WHERE WhsRefID = @WhsRefID";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@WhsRefID", WhsRefID);                
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    rowsCount = sda.Fill(dt);
                    retval = "Successfully Selected ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.newID = newID;
                rst.retval = retval;
                rst.rowCount = rowsCount;
                rst.WhsRefID = WhsRefID;

                if (dt.Rows.Count == 1)
                {
                    rst.WhsCode = dt.Rows[0]["WhsCode"].ToString();
                }

            }

            return rst;
        }

        #endregion

        #region '-- GetBillType --'
        public dynamic GetBillType()
        {
            DataTable dt = new DataTable();
            dynamic rst = new ExpandoObject();
            dynamic ObjWarehouse = new ExpandoObject();
            dynamic ObjJob = new ExpandoObject();
            string retval;
            int rowid = 0;
            int rowsCount = 0;
            rst.WhsCode = 0;
            rst.rowid = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM dbo.tbl_SetupOrderItemBillType WHERE BillTypeID LIKE @BillTypeID";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@BillTypeID", "%billing%");
                connection.Open();
                try
                {
                    rowid = Convert.ToInt32(command.ExecuteScalar());
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    rowsCount = sda.Fill(dt);
                    retval = "Successfully Selected ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.rowid = rowid;
                rst.retval = retval;
                rst.rowCount = rowsCount;              

            }

            return rst;
        }

        #endregion

        public dynamic updatePrefixDocNo(dynamic a)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "UPDATE dbo.tbl_PalmsysPrefix SET SerialNo = @SerialNo , UpdOn = GETDATE()  WHERE ID = @ID;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@SerialNo", a.NewSerialNo);
                command.Parameters.AddWithValue("@ID", a.newID);
                connection.Open();
                try
                {
                    newID = command.ExecuteNonQuery(); ;
                    retval = "Successfully Updated ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.rowID = newID;
                rst.retval = retval;
                return rst;
            }
        }

        #region '-- InsertJobItem --'
        public dynamic InsertJobItem(dynamic a)
        {
            //dynamic a = new ExpandoObject();
            dynamic rst = new ExpandoObject();
            string retval;
            int newID = 0;
            rst.newID = 0;
            //a.DocRefID = DocRefID;
            //a.ItemClientRefID = 45;
            //a.ExpcQty = 5.00;
            //a.CtryRefID = 17;
            a.Remarks = "";
            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "INSERT INTO dbo.tbl_OrderJobItem (DocRefID  , BillTypeRefID  , ItemClientRefID  , ExpcQty  , PickQty , CtryRefID , UnitPrice,  Remarks  , UpdBy  , UpdOn)" +
                    " VALUES (@DocRefID , @BillTypeRefID , @ItemClientRefID , @ExpcQty , @PickQty , @CtryRefID , @UnitPrice ,@Remarks  , @UpdBy  ,  GETDATE()); SELECT SCOPE_IDENTITY();";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@DocRefID", a.DocRefID);
                command.Parameters.AddWithValue("@BillTypeRefID", a.billtype);
                command.Parameters.AddWithValue("@ItemClientRefID", a.ItemClientRefID);
                command.Parameters.AddWithValue("@ExpcQty", a.ExpcQty);
                command.Parameters.AddWithValue("@PickQty", 0);
                command.Parameters.AddWithValue("@CtryRefID", a.CtryRefID);
                command.Parameters.AddWithValue("@UnitPrice", a.UnitPrice);
                command.Parameters.AddWithValue("@Remarks", a.Remarks);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                connection.Open();
                try
                {
                    rst.newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Inserted ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                //rst.newID = newID;
                rst.retval = retval;
                return rst;
            }
        }
        #endregion

        #region '-- InsertJobDetail --'
        public dynamic InsertJobDetail(int JobRefID , dynamic a)
        {
            //dynamic a = new ExpandoObject();
            dynamic rst = new ExpandoObject();
            string retval;
            int newID = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "INSERT INTO dbo.tbl_OrderJobDetail (JobRefID  , GoodsType  ,  ShippingUnit  , Remarks , IsScanning  , IsUpload , UpdBy  , UpdOn )" +
                    " VALUES (@JobRefID  , @GoodsType  ,  @ShippingUnit , @Remarks  , @IsScanning  , @IsUpload , @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@JobRefID", JobRefID);
                command.Parameters.AddWithValue("@GoodsType", "NB");
                command.Parameters.AddWithValue("@ShippingUnit", 0);
                command.Parameters.AddWithValue("@Remarks", a.order.customer_note.ToString());
                command.Parameters.AddWithValue("@IsScanning", 0);
                command.Parameters.AddWithValue("@IsUpload", 0);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Inserted ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.newID = newID;
                rst.retval = retval;
                return rst;
            }
        }
        #endregion

        #region '-- GetAndSetJobID --'
        public dynamic GetAndSetJobID(dynamic a)
        {
            DataTable dt = new DataTable();
            dynamic rst = new ExpandoObject();
            dynamic ObjWarehouse = new ExpandoObject();
            dynamic ObjWhsCode = new ExpandoObject();
            dynamic ObjJob = new ExpandoObject();
            string retval;
            int newID = 0;

            //a.StateCode = "PNG";
            dynamic findWarehouse = new ExpandoObject();
            findWarehouse = findWarehouseJson(a.StateCode);
            if (!findWarehouse.statename.Equals("0")) {
                ObjWarehouse = getWhsRefID(findWarehouse.statename);
                rst.ObjWarehouse = ObjWarehouse;
                if (ObjWarehouse != null)
                {
                    if (ObjWarehouse.rowID > 0)
                    {
                        rst.WhsRefID = ObjWarehouse.rowID;
                        rst.WhsID = findWarehouse.statename;
                        string aa = ""; int rowsCount = 0;
                        using (SqlConnection connection = new SqlConnection(cons))
                        {
                            string PrefixSTR = "OT";
                            string queryString = "SELECT * FROM dbo.tbl_PalmsysPrefix WHERE WhsRefID = @WhsRefID AND PrefixID = @PrefixID";
                            SqlCommand command = new SqlCommand(queryString, connection);
                            command.Parameters.AddWithValue("@WhsRefID", rst.WhsRefID);
                            command.Parameters.AddWithValue("@PrefixID", PrefixSTR);
                            connection.Open();
                            try
                            {
                                newID = Convert.ToInt32(command.ExecuteScalar());
                                SqlDataAdapter sda = new SqlDataAdapter(command);
                                rowsCount = sda.Fill(dt);
                                retval = "Successfully Selected ";
                            }
                            catch (SqlException e) { retval = e.Message; }
                            if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                            rst.newID = newID;
                            rst.retval = retval;
                            rst.rowCount = rowsCount;

                            if (dt.Rows.Count == 1)
                            {
                                rst.NewSerialNo = (int)dt.Rows[0]["SerialNo"] + 1;
                                rst.SerialNo = dt.Rows[0]["SerialNo"].ToString();
                                rst.SerialDigit = dt.Rows[0]["SerialDigit"].ToString();
                                rst.StringSerialNo = Convert.ToString(rst.NewSerialNo).PadLeft(int.Parse(rst.SerialDigit), '0');
                                rst.JobID = ObjWarehouse.WhsCode + PrefixSTR + rst.WhsRefID + "" + rst.StringSerialNo;
                                ObjJob = updatePrefixJob(rst);
                                rst.UpdateJob = ObjJob;
                            }

                        }
                    }
                }

            }

            #region old warehouse
            //if (WarehouseState.ContainsKey(a.StateCode))
            //{
            //    if (!WarehouseState[a.StateCode].Trim().Equals(""))
            //    {
            //        ObjWarehouse = getWhsRefID(WarehouseState[a.StateCode].Trim());
            //        rst.ObjWarehouse = ObjWarehouse;
            //        if (ObjWarehouse != null)
            //        {
            //            if (ObjWarehouse.rowID > 0)
            //            {
            //                rst.WhsRefID = ObjWarehouse.rowID;
            //                rst.WhsID = WarehouseState[a.StateCode].Trim();
            //                string aa = ""; int rowsCount = 0;
            //                using (SqlConnection connection = new SqlConnection(cons))
            //                {
            //                    string queryString = "SELECT * FROM dbo.tbl_PalmsysPrefix WHERE WhsRefID = @WhsRefID AND PrefixID = @PrefixID";
            //                    SqlCommand command = new SqlCommand(queryString, connection);
            //                    command.Parameters.AddWithValue("@WhsRefID", rst.WhsRefID);
            //                    command.Parameters.AddWithValue("@PrefixID", "IN");
            //                    connection.Open();
            //                    try
            //                    {
            //                        newID = Convert.ToInt32(command.ExecuteScalar());
            //                        SqlDataAdapter sda = new SqlDataAdapter(command);
            //                        rowsCount = sda.Fill(dt);
            //                        retval = "Successfully Selected ";
            //                    }
            //                    catch (SqlException e) { retval = e.Message; }
            //                    if (connection.State == System.Data.ConnectionState.Open) connection.Close();
            //                    rst.newID = newID;
            //                    rst.retval = retval;
            //                    rst.rowCount = rowsCount;

            //                    if (dt.Rows.Count == 1)
            //                    {
            //                        rst.NewSerialNo = (int)dt.Rows[0]["SerialNo"] + 1;
            //                        rst.SerialNo = dt.Rows[0]["SerialNo"].ToString();
            //                        rst.SerialDigit = dt.Rows[0]["SerialDigit"].ToString();
            //                        rst.StringSerialNo = Convert.ToString(rst.NewSerialNo).PadLeft(int.Parse(rst.SerialDigit), '0');
            //                        rst.JobID = ObjWarehouse.WhsCode+"IN" + rst.WhsRefID + "" + rst.StringSerialNo;
            //                        ObjJob = updatePrefixJob(rst);
            //                        rst.UpdateJob = ObjJob;
            //                    }

            //                }
            //            }
            //        }
            //    }

            //}
            #endregion
            return rst;
        }

        #endregion

        public dynamic updatePrefixJob(dynamic a)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "UPDATE dbo.tbl_PalmsysPrefix SET SerialNo = @SerialNo , UpdOn = GETDATE()  WHERE ID = @ID;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@SerialNo", a.NewSerialNo);
                command.Parameters.AddWithValue("@ID", a.newID);
                connection.Open();
                try
                {
                    newID = command.ExecuteNonQuery(); ;
                    retval = "Successfully Updated ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.rowID = newID;
                rst.retval = retval;
                return rst;
            }
        }

        public dynamic getWhsRefID(string WhsID)
        {
            dynamic rst = new ExpandoObject();
            int rowsCount = 0;
            string retval;
            int newID = 0;
            DataTable dt = new DataTable();
            //  string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();
            rst.WhsCode = "00";
            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupWareHouse WHERE WhsID = @WhsID;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@WhsID", WhsID);
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    rowsCount = sda.Fill(dt);

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.rowID = newID;
                rst.retval = retval;

                if (dt.Rows.Count == 1)
                {
                    rst.WhsCode = dt.Rows[0]["WhsCode"].ToString();
                }
                return rst;

            }

            //return rst;
        }

        public dynamic getStateRefID(string State)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;
            //  string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupCountryState WHERE State = @State;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@State", State);
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.rowID = newID;
                rst.retval = retval;
                return rst;

            }

            //return rst;
        }

        public dynamic getConsignorClientRefID(dynamic Obj)
        {
            dynamic rst = new ExpandoObject();
            string WhsID = Obj.WhsID; int WhsRefID = Obj.WhsRefID;
            string retval;
            int newID = 0;
            //  string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupClient WHERE ClientID = @ClientID AND WhsRefID = @WhsRefID;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientID", "MED" + WhsID);
                command.Parameters.AddWithValue("@WhsRefID", WhsRefID);
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.rowID = newID;
                rst.retval = retval;
                return rst;

            }

            //return rst;
        }

        public dynamic getConsigneeClientRefID(string clientID)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;
            //  string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();
            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupClient WHERE ClientID = @ClientID AND Role = @Role;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientID", clientID);
                command.Parameters.AddWithValue("@Role", "EC");
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.rowID = newID;
                rst.retval = retval;
                // return rst;

            }
            return rst;
        }

        public dynamic getOriClientAddRefID(dynamic a)
        {
            dynamic rst = new ExpandoObject();
            string retval; string retval2;
            int newID = 0; int newID2 = 0;
            //  string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();
            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupClientAddress WHERE ClientRefID = @ClientRefID AND IsShip = @IsShip;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", a.ConsignorClientRefID);
                command.Parameters.AddWithValue("@IsShip", 0);
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();

                rst.OriClientAddRefID = newID; //OriClientAddRefID
                rst.retval_ori = retval;
                return rst;
            }            
            return rst;
        }

        #region 'getAddressRefIDBAK // GET BOTH  OriClientAddRefID & DestClientAddRefID'
        public dynamic getAddressRefIDBAK(dynamic a)
        {
            dynamic rst = new ExpandoObject();
            string retval; string retval2;
            int newID = 0; int newID2 = 0;
            //  string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();
            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupClientAddress WHERE ClientRefID = @ClientRefID AND IsShip = @IsShip;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", a.ConsignorClientRefID);
                command.Parameters.AddWithValue("@IsShip", 0);
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();

                rst.OriClientAddRefID = newID; //OriClientAddRefID
                rst.retval_ori = retval;
                // return rst;

            }

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupClientAddress WHERE ClientRefID = @ClientRefID AND IsShip = @IsShip;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", a.ConsigneeClientRefID);
                command.Parameters.AddWithValue("@IsShip", 0);
                connection.Open();
                try
                {
                    newID2 = Convert.ToInt32(command.ExecuteScalar());
                    retval2 = "Successfully Select ";
                }
                catch (SqlException e) { retval2 = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();

                rst.DestClientAddRefID = newID2; //ConsigneeClientRefID
                rst.retval_dest = retval2;
                // return rst;

            }
            return rst;
        }
        #endregion

        public dynamic GetItemRefID(string product_id)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;
            //  string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupItem WHERE ItemID = @ItemID;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ItemID", product_id);
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.ItemRefID = newID;
                rst.retval = retval;
                return rst;

            }

            //return rst;
        }

        public dynamic GetItemClientRefID(int ItemRefID)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;
            //  string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupItemClient WHERE ItemRefID = @ItemRefID;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ItemRefID", ItemRefID);
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.ItemClientRefID = newID;
                rst.retval = retval;
                return rst;

            }

            //return rst;
        }
        public dynamic OrderedProduct(dynamic product)
        {
            dynamic rst = new ExpandoObject();

          
            var DyObjectsList = new List<dynamic>();

            foreach (dynamic i in product.details)
            {
                dynamic dt = new ExpandoObject();
                foreach (dynamic ab in i)
                {
                    dt.product_id = ab.product_id;
                    dt.qty = ab.qty;
                //    dt.line_subtotal = ab.line_subtotal;
                    dt.item_product_id = 0;
                    foreach (dynamic pi in product.product_id)
                    {
                        if (pi.Name.Equals(ab.product_id.ToString()))
                        {
                            dt.item_product_id = pi.Value.id.ToString();
                            dt.price = pi.Value.price.ToString();
                        }

                    }
                    dynamic ItemRefID = GetItemRefID(dt.item_product_id.ToString());
                    dt.ItemRefID = ItemRefID.ItemRefID;
                    dynamic ItemClientRefID = GetItemClientRefID(dt.ItemRefID);
                    dt.ItemClientRefID = ItemClientRefID.ItemClientRefID;

                   

                }
                DyObjectsList.Add(dt);
                

            }
            
            rst.data = DyObjectsList;

            return rst;
        }

        public dynamic OrderedProduct3(dynamic product)
        {
            dynamic rst = new ExpandoObject();
            string product_type = product.details.product_type.ToString();
            string var_product_id;

            if (product_type.Equals("variation"))
            {
                var_product_id = product.var_product_id.ToString();
                rst.ItemRefID = GetItemRefID(var_product_id);
                rst.ItemClientRefID = GetItemClientRefID(rst.ItemRefID.ItemRefID);
            }
            return rst;
        }

        public dynamic getCtryRefID(string value)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;
            //  string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();

            using (SqlConnection connection = new SqlConnection(cons))
            {
                
                string queryString = "SELECT * FROM  dbo.tbl_SetupCountry WHERE CtryID = @CtryID;";
                //string queryString = "SELECT * FROM  dbo.tbl_SetupCountry WHERE CurrencyID = @CurrencyID;";
                SqlCommand command = new SqlCommand(queryString, connection);
              //  command.Parameters.AddWithValue("@CurrencyID", CurrencyID);
                command.Parameters.AddWithValue("@CtryID", value);
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.CtryRefID = newID;
                rst.retval = retval;
                return rst;

            }

            //return rst;
        }

        #region "InsertShippingAddress"
        public dynamic InsertShippingAddress(dynamic a, dynamic ObjRegCusdt, int StateRefID)
        {
            dynamic rst = new ExpandoObject();
            string retval; string retval2;
            int newID = 0; int newID2 = 0;

            rst.DestClientAddRefID = "0";

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "INSERT INTO dbo.tbl_SetupClientAddress (ClientRefID , IsShip , ShipToName , ContactPerson , Telephone1 , Telephone2 ," +
                    "Fax , Address , PostCode , City , StateRefID ,   UpdBy , UpdOn)" +
                    " VALUES (@ClientRefID , @IsShip , @ShipToName , @ContactPerson , @Telephone1 , @Telephone2 , @Fax , @Address , @PostCode , @City , @StateRefID ,   @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";

                SqlCommand command = new SqlCommand(queryString, connection);
                command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", ObjRegCusdt.rowID.ToString());
                command.Parameters.AddWithValue("@IsShip", 1);
                command.Parameters.AddWithValue("@ShipToName", a.customerinfo.shipping.first_name.ToString() + " , " + a.customerinfo.shipping.last_name.ToString());
                command.Parameters.AddWithValue("@ContactPerson", "");
                command.Parameters.AddWithValue("@Telephone1", a.customerinfo.billing.phone.ToString());
                command.Parameters.AddWithValue("@Telephone2", "");
                command.Parameters.AddWithValue("@Fax", "");
                command.Parameters.AddWithValue("@Address", a.customerinfo.shipping.address_1.ToString() + " , " + a.customerinfo.shipping.address_2.ToString());
                command.Parameters.AddWithValue("@PostCode", a.customerinfo.shipping.postcode.ToString());
                command.Parameters.AddWithValue("@City", a.customerinfo.shipping.city.ToString());
                command.Parameters.AddWithValue("@StateRefID", StateRefID);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");

                connection.Open();
                try
                {
                    rst.DestClientAddRefID = Convert.ToInt32(command.ExecuteScalar());
                    retval2 = "Successfully Select ";
                }
                catch (SqlException e) { retval2 = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();

            }
            return rst;
        }
        #endregion

        #region "InsertBillingAddress"
        public dynamic InsertBillingAddress(dynamic a, dynamic ObjRegCusdt, int StateRefID) {
            dynamic rst = new ExpandoObject();
            string retval; string retval2;
            int newID = 0; int newID2 = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "INSERT INTO dbo.tbl_SetupClientAddress (ClientRefID , IsShip , ShipToName , ContactPerson , Telephone1 , Telephone2 ," +
                    "Fax , Address , PostCode , City , StateRefID ,   UpdBy , UpdOn)" +
                    " VALUES (@ClientRefID , @IsShip , @ShipToName , @ContactPerson , @Telephone1 , @Telephone2 , @Fax , @Address , @PostCode , @City , @StateRefID ,   @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";
                
                SqlCommand command = new SqlCommand(queryString, connection);               
                command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", ObjRegCusdt.rowID.ToString());
                command.Parameters.AddWithValue("@IsShip", 0);
                command.Parameters.AddWithValue("@ShipToName", a.customerinfo.billing.first_name.ToString() + " , " + a.customerinfo.billing.last_name.ToString());
                command.Parameters.AddWithValue("@ContactPerson", "");
                command.Parameters.AddWithValue("@Telephone1", a.customerinfo.billing.phone.ToString());
                command.Parameters.AddWithValue("@Telephone2", "");
                command.Parameters.AddWithValue("@Fax", "");
                command.Parameters.AddWithValue("@Address", a.customerinfo.billing.address_1.ToString() + " , " + a.customerinfo.billing.address_2.ToString());
                command.Parameters.AddWithValue("@PostCode", a.customerinfo.billing.postcode.ToString());
                command.Parameters.AddWithValue("@City", a.customerinfo.billing.city.ToString());
                command.Parameters.AddWithValue("@StateRefID", StateRefID);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
               
                connection.Open();
                try
                {
                    newID2 = Convert.ToInt32(command.ExecuteScalar());
                    retval2 = "Successfully Select ";
                }
                catch (SqlException e) { retval2 = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();               

            }
            return rst;
        }
        #endregion

        #region UpdateBillingAddress
        public dynamic UpdateBillingAddress(dynamic a, dynamic ObjRegCusdt, int StateRefID)
        {
            dynamic rst = new ExpandoObject();
            string retval; string retval2;
            int newID = 0; rst.newID2 = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "UPDATE dbo.tbl_SetupClientAddress SET IsShip = @IsShip , ShipToName = @ShipToName , ContactPerson = @ContactPerson  , Telephone1 = @Telephone1, Telephone2 = @Telephone2 ," +
                   "Fax =  @Fax , Address = @Address , PostCode = @PostCode , City = @City , StateRefID =  @StateRefID  ,   UpdBy =  @UpdBy , UpdOn = GETDATE() WHERE ClientRefID = @ClientRefID ; SELECT SCOPE_IDENTITY();";
                
                
                SqlCommand command = new SqlCommand(queryString, connection);
                command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", ObjRegCusdt.rowID.ToString());
                command.Parameters.AddWithValue("@IsShip", 0);
                command.Parameters.AddWithValue("@ShipToName", a.customerinfo.billing.first_name.ToString() + " , " + a.customerinfo.billing.last_name.ToString());
                command.Parameters.AddWithValue("@ContactPerson", "");
                command.Parameters.AddWithValue("@Telephone1", a.customerinfo.billing.phone.ToString());
                command.Parameters.AddWithValue("@Telephone2", "");
                command.Parameters.AddWithValue("@Fax", "");
                command.Parameters.AddWithValue("@Address", a.customerinfo.billing.address_1.ToString() + " , " + a.customerinfo.billing.address_2.ToString());
                command.Parameters.AddWithValue("@PostCode", a.customerinfo.billing.postcode.ToString());
                command.Parameters.AddWithValue("@City", a.customerinfo.billing.city.ToString());
                command.Parameters.AddWithValue("@StateRefID", StateRefID);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
              //  command.Parameters.AddWithValue("@ClientRefID", ObjRegCusdt.rowID.ToString());

                connection.Open();
                try
                {
                    rst.newID2 = command.ExecuteScalar();
                    retval2 = "Successfully Select ";
                }
                catch (SqlException e) { retval2 = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();

            }
            return rst;
        }
        #endregion

        #region "getUserBillingAddressRefID"
        public dynamic getUserBillingAddressRefID(dynamic a, dynamic ObjRegCusdt , int StateRefID)
        {
            dynamic rst = new ExpandoObject();
            string retval; string retval2;
            int newID = 0; int newID2 = 0;           
           
            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupClientAddress WHERE ClientRefID = @ClientRefID AND IsShip = @IsShip;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", ObjRegCusdt.rowID);
                command.Parameters.AddWithValue("@IsShip", 0);
                connection.Open();
                try
                {
                    newID2 = Convert.ToInt32(command.ExecuteScalar());
                    retval2 = "Successfully Select ";
                }
                catch (SqlException e) { retval2 = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                if (newID2 > 0)
                {
                    rst.DestClientAddRefID = newID2; //ConsigneeClientRefID
                    rst.retval_dest = retval2;
                }
                else {
                    InsertBillingAddress(a, ObjRegCusdt , StateRefID);
                }               
                // return rst;

            }
            return rst;
        }
        #endregion

        #region "getUserShippingAddressRefID"
        public dynamic getUserShippingAddressRefID(dynamic a, dynamic ObjRegCusdt, int StateRefID)
        {
            dynamic rst = new ExpandoObject();
            string retval; string retval2;
            int newID = 0; int newID2 = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupClientAddress WHERE ClientRefID = @ClientRefID AND IsShip = @IsShip;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", ObjRegCusdt.rowID);
                command.Parameters.AddWithValue("@IsShip", 1);
                connection.Open();
                try
                {
                    newID2 = Convert.ToInt32(command.ExecuteScalar());
                    retval2 = "Successfully Select ";
                }
                catch (SqlException e) { retval2 = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                if (newID2 > 0)
                {
                    rst.DestClientAddRefID = newID2; //ConsigneeClientRefID
                    rst.retval_dest = retval2;
                }
                else
                {
                    InsertBillingAddress(a, ObjRegCusdt, StateRefID);
                }
                // return rst;

            }
            return rst;
        }
        #endregion

        public dynamic findStateJson(string codeState)
        {
            dynamic rst = new ExpandoObject(); dynamic array = new ExpandoObject();
            string jsonread = "";
            using (StreamReader r = new StreamReader(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/file/data.json"))
            {
                jsonread = r.ReadToEnd();
                JavaScriptSerializer ser = new JavaScriptSerializer();
                array = JsonConvert.DeserializeObject(jsonread);
            }

            rst.statename = 0;
            if (array != null) {
                foreach (dynamic st in array.State)
                {
                    if (st.Name.Equals(codeState)) {
                        rst.statename = st.Value.ToString();
                    }
                }
            }

            return rst;
        }

        public dynamic findWarehouseJson(string codeState)
        {
            dynamic rst = new ExpandoObject(); dynamic array = new ExpandoObject();
            string jsonread = "";
            using (StreamReader r = new StreamReader(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/file/data.json"))
            {
                jsonread = r.ReadToEnd();
                JavaScriptSerializer ser = new JavaScriptSerializer();
                array = JsonConvert.DeserializeObject(jsonread);
            }

            rst.statename = "0";
            if (array != null)
            {
                foreach (dynamic st in array.Warehouse)
                {
                    if (st.Name.Equals(codeState))
                    {
                        rst.statename = st.Value.ToString();
                    }
                }
            }

            return rst;
        }


        #region '-- CheckOrderExists --'
        public dynamic CheckOrderExists(string orderid)
        {
            DataTable dt = new DataTable();
            dynamic rst = new ExpandoObject();
            dynamic ObjWarehouse = new ExpandoObject();
            dynamic ObjJob = new ExpandoObject();
            string retval;
            int newID = 0;
            int rowsCount = 0;
            rst.RowOrderID = 0;


            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM dbo.tbl_OrderJobDocument WHERE DocNo = @DocNo";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@DocNo", "EC"+orderid);
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    rowsCount = sda.Fill(dt);
                    retval = "Successfully Selected ";
                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.newID = newID;
                rst.retval = retval;
                rst.rowCount = rowsCount;
                rst.RowOrderID = orderid;                                

            }

            return rst;
        }

        #endregion

        #region '-- warehouse location --'
        //Dictionary<string, string> WarehouseState = new Dictionary<string, string> {
        //    { "JHR", "KL" },
        //    { "KDH", "PG" },
        //    { "KTN", "KL" },
        //    { "LBN", "KL" },
        //    { "MLK", "KL" },
        //    { "NSN", "KL" },
        //    { "PHG", "KL" },
        //    { "PNG", "PG" },
        //    { "PRK", "PG" },
        //    { "PLS", "PG" },
        //    { "SBH", "KL" },
        //    { "SWK", "KL" },
        //    { "SGR", "KL" },
        //    { "TRG", "KL" },
        //    { "PJY", "KL" },
        //    { "KUL", "KL" }
        //};
        #endregion

        public int checkTaxFree(dynamic a)
        {
            int def = 1;
            string city = a.customerinfo.shipping.city.ToString().ToLower();
            string state = a.customerinfo.shipping.state.ToString().ToLower();
            string postcode = a.customerinfo.shipping.postcode.ToString();
            postcode = Regex.Replace(postcode, @"\s+", "");

            if (state.Equals("kdh") && city.Equals("langkawi") || postcode.Equals("07000") ||
                postcode.Equals("07007") || postcode.Equals("07009")) {
                def = 0;
            }
            
            return def;
        }

        #region RegisterNewCustomer
        public dynamic RegisterNewCustomer(dynamic a, int isTax)
        {
            dynamic rst = new ExpandoObject();
            string retval;

            rst.newID = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "INSERT INTO dbo.tbl_SetupClient (ClientID , ClientName , Email1, Role , BGAmt , IsActive, IsAutoPicking ,"
      + "IsEmailAlert, IsPalletizing , IsTax , UpdBy ,UpdOn )" +
                    " VALUES (@ClientID , @ClientName , @Email1 , @Role , @BGAmt , @IsActive, @IsAutoPicking ,"
      + "@IsEmailAlert, @IsPalletizing , @IsTax ,  @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientID", a.userid);
                command.Parameters.AddWithValue("@ClientName", a.name);
                command.Parameters.AddWithValue("@Email1", a.user_email);
                command.Parameters.AddWithValue("@Role", a.role);
                command.Parameters.AddWithValue("@BGAmt", "0.00000");
                command.Parameters.AddWithValue("@IsActive", "1");
                command.Parameters.AddWithValue("@IsAutoPicking", "0");
                command.Parameters.AddWithValue("@IsEmailAlert", "0");
                command.Parameters.AddWithValue("@IsPalletizing", "0");
                command.Parameters.AddWithValue("@IsTax", isTax);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");

                connection.Open();
                try
                {
                    // command.Parameters.Clear();
                    // command.CommandText = "SELECT @@IDENTITY";
                    rst.newID = command.ExecuteScalar();
                    //  newID = command.ExecuteNonQuery();

                    retval = "Successfully Inserted ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                //rst.newID = newID;
                rst.retval = retval;
                return rst;

            }
        }
        #endregion

        #region UpdateCustomer
        public dynamic UpdateCustomer(dynamic ObjRegCusdt, int isTax)
        {
            dynamic rst = new ExpandoObject();
            string retval;

            rst.newID = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "UPDATE dbo.tbl_SetupClient SET IsTax = @IsTax , UpdOn = GETDATE() WHERE  ClientRefID = @ClientRefID; SELECT SCOPE_IDENTITY();";                   
                               
                SqlCommand command = new SqlCommand(queryString, connection);                
                command.Parameters.AddWithValue("@IsTax", isTax);
                command.Parameters.AddWithValue("@ClientRefID", ObjRegCusdt.rowID.ToString());

                connection.Open();
                try
                {                    
                    rst.newID = command.ExecuteScalar();                  
                    retval = "Successfully Inserted ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();               
                rst.retval = retval;
                return rst;

            }
        }
        #endregion

    }
}