﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;



namespace vtm.Customers
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICustomers" in both code and config file together.
    [ServiceContract]
    public interface ICustomers
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "RegisterNewCustomer")]
        Stream RegisterNewCustomer(Stream streamdata);

        [OperationContract]
        [WebInvoke(Method = "POST",
           ResponseFormat = WebMessageFormat.Json,
           RequestFormat = WebMessageFormat.Json,
           UriTemplate = "EditAddress")]
        Stream EditAddress(Stream streamdata);

        [OperationContract]
        [WebInvoke(Method = "POST",
           ResponseFormat = WebMessageFormat.Json,
           RequestFormat = WebMessageFormat.Json,
           UriTemplate = "trytest")]
        Stream trytest(Stream streamdata);

        [OperationContract]
        [WebInvoke(Method = "POST",
           ResponseFormat = WebMessageFormat.Json,
           RequestFormat = WebMessageFormat.Json,
           UriTemplate = "EditCustomer")]
        Stream EditCustomer(Stream streamdata);

    }
}
