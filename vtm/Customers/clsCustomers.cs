﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace vtm.Customers
{
    public class clsCustomers
    {
        string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();
        public dynamic RegisterNewCustomer(dynamic a)
        {
            dynamic rst = new ExpandoObject();
            string retval;

            rst.newID = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "INSERT INTO dbo.tbl_SetupClient (ClientID , ClientName , Email1, Role , BGAmt , IsActive, IsAutoPicking ,"
      + "IsEmailAlert, IsPalletizing , IsTax , UpdBy ,UpdOn )" +
                    " VALUES (@ClientID , @ClientName , @Email1 , @Role , @BGAmt , @IsActive, @IsAutoPicking ,"
      + "@IsEmailAlert, @IsPalletizing , @IsTax ,  @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";
               
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientID", a.userid);
                command.Parameters.AddWithValue("@ClientName", a.name);
                command.Parameters.AddWithValue("@Email1", a.user_email);
                command.Parameters.AddWithValue("@Role", a.role);
                command.Parameters.AddWithValue("@BGAmt", "0.00000");
                command.Parameters.AddWithValue("@IsActive", "1");
                command.Parameters.AddWithValue("@IsAutoPicking", "0");
                command.Parameters.AddWithValue("@IsEmailAlert", "0");
                command.Parameters.AddWithValue("@IsPalletizing", "0");
                command.Parameters.AddWithValue("@IsTax", 1);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
               
                connection.Open();
                try
                {
                    // command.Parameters.Clear();
                    // command.CommandText = "SELECT @@IDENTITY";
                    rst.newID = command.ExecuteScalar();
                    //  newID = command.ExecuteNonQuery();

                    retval = "Successfully Inserted ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                //rst.newID = newID;
                rst.retval = retval;
                return rst;

            }
        }

        public dynamic EditCustomer(dynamic a)
        {
            dynamic rst = new ExpandoObject();
            string retval;

            rst.newID = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "UPDATE dbo.tbl_SetupClient  SET  ClientName = @ClientName , Email1 = @Email1 , UpdOn = GETDATE() WHERE ClientID = @ClientID;"
                                    + "SELECT SCOPE_IDENTITY();";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientID", a.userid);
                command.Parameters.AddWithValue("@ClientName", a.name);
                command.Parameters.AddWithValue("@Email1", a.user_email);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                connection.Open();
                try
                {
                    rst.newID = command.ExecuteScalar();
                    retval = "Successfully Inserted ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();

                rst.retval = retval;
                return rst;

            }
        }

        public dynamic InsertClientUDF(int clientRefID)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;


            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "INSERT INTO dbo.tbl_SetupClientUDF (ClientRefID , UpdBy , UpdOn,  IsScanningUDF1 , IsScanningUDF2 , IsScanningUDF3, IsScanningUDF4 ,"
      + "IsScanningUDF5, IsScanningUDF6 , IsScanningUDF7 , IsScanningUDF8 , IsScanningUDF9 , IsScanningUDF10)" +
                    " VALUES (@ClientRefID , @UpdBy ,  GETDATE() ,  @IsScanningUDF1 , @IsScanningUDF2 , @IsScanningUDF3, @IsScanningUDF4 ,"
      + "@IsScanningUDF5, @IsScanningUDF6 ,  @IsScanningUDF7 , @IsScanningUDF8 , @IsScanningUDF9 , @IsScanningUDF10 ); SELECT SCOPE_IDENTITY();";
                
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", clientRefID);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");               
                command.Parameters.AddWithValue("@IsScanningUDF1", 0);
                command.Parameters.AddWithValue("@IsScanningUDF2", 0);
                command.Parameters.AddWithValue("@IsScanningUDF3", 0);
                command.Parameters.AddWithValue("@IsScanningUDF4", 0);
                command.Parameters.AddWithValue("@IsScanningUDF5", 0);
                command.Parameters.AddWithValue("@IsScanningUDF6", 0);
                command.Parameters.AddWithValue("@IsScanningUDF7", 0);
                command.Parameters.AddWithValue("@IsScanningUDF8", 0);
                command.Parameters.AddWithValue("@IsScanningUDF9", 0);
                command.Parameters.AddWithValue("@IsScanningUDF10", 0);

                connection.Open();
                try
                {
                    // command.Parameters.Clear();
                    // command.CommandText = "SELECT @@IDENTITY";
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    //  newID = command.ExecuteNonQuery();

                    retval = "Successfully Inserted ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.newID = newID;
                rst.retval = retval;
                return rst;

            }

            //return rst;
        }

        public dynamic getClientRefID(string clientID)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;


            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupClient WHERE Role = @Role AND ClientID = @ClientID;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Role", "EC");
                command.Parameters.AddWithValue("@ClientID", clientID);

                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.rowID = newID;
                rst.retval = retval;
                return rst;

            }

            //return rst;
        }

        public dynamic getClientAddRefID(int clientRefID, int isship)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;

            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupClientAddress WHERE IsShip = @IsShip AND ClientRefID = @ClientRefID;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@IsShip", isship);
                command.Parameters.AddWithValue("@ClientRefID", clientRefID);
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.rowID = newID;
                rst.retval = retval;
                return rst;

            }

            //return rst;
        }

        public dynamic getStateRefID(string State)
        {
            dynamic rst = new ExpandoObject();

            string retval;
            int newID = 0;


            using (SqlConnection connection = new SqlConnection(cons))
            {
                string queryString = "SELECT * FROM  dbo.tbl_SetupCountryState WHERE State LIKE @State;";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@State", "%" + State + "%");

                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Select ";

                }
                catch (SqlException e) { retval = e.Message; }
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.rowID = newID;
                rst.retval = retval;
                return rst;

            }

            //return rst;
        }

        #region findStateJson
        public dynamic findStateJson(string codeState)
        {
            dynamic rst = new ExpandoObject(); dynamic array = new ExpandoObject();
            string jsonread = "";
            using (StreamReader r = new StreamReader(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/file/data.json"))
            {
                jsonread = r.ReadToEnd();
                JavaScriptSerializer ser = new JavaScriptSerializer();
                array = JsonConvert.DeserializeObject(jsonread);
            }

            rst.statename = "0";
            if (array != null)
            {
                foreach (dynamic st in array.State)
                {
                    if (st.Name.Equals(codeState))
                    {
                        rst.statename = st.Value.ToString();
                    }
                }
            }

            return rst;
        }
        #endregion

    }
}