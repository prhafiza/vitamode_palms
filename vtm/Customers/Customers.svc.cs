﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using vtm.Common;

namespace vtm.Customers
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Customers" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Customers.svc or Customers.svc.cs at the Solution Explorer and start debugging.
    public class Customers : ICustomers
    {
        clsCustomers clsCustomers = new clsCustomers();
        clsCommon clsCommon = new clsCommon();
        string cons = ConfigurationManager.ConnectionStrings["palms"].ToString();

        public Stream trytest(Stream streamdata)
        {
            StreamReader reader = new StreamReader(streamdata);
            string res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();
            var @params = HttpUtility.ParseQueryString(res);
            //string user_email = @params["data[user_email]"];
            string role = "EC";

            var DyObjectsList = new List<dynamic>();
            dynamic DyObj = new ExpandoObject();
            //   DyObj.user_email = user_email.Trim();
            DyObj.role = role;

            dynamic objReg = new ExpandoObject();
            dynamic objUDF = new ExpandoObject();
            objReg = clsCustomers.getClientRefID("MEDPG");

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(new { objReg = objReg });

            return new MemoryStream(Encoding.UTF8.GetBytes(clsCommon.aaSerializeJsona(json)));
        }

        #region "----RegisterNewCustomer----"
        public Stream RegisterNewCustomer(Stream streamdata)
        {
            StreamReader reader = new StreamReader(streamdata);
            string res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();
            var @params = HttpUtility.ParseQueryString(res);
            string user_email = @params["user_email"];
            string userid = @params["userid"];
            string role = "EC";

            var DyObjectsList = new List<dynamic>();
            dynamic DyObj = new ExpandoObject();
            DyObj.user_email = user_email.Trim();
            DyObj.userid = userid.Trim();
            DyObj.role = role;
            DyObj.name = user_email.Trim();

            dynamic objReg = new ExpandoObject();
            dynamic objUDF = new ExpandoObject();
            objReg = clsCustomers.RegisterNewCustomer(DyObj);

            if (objReg != null)
            {
                objReg.newID = Convert.ToInt32(objReg.newID);
                if (objReg.newID > 0)
                {
                    objUDF = clsCustomers.InsertClientUDF(objReg.newID);
                }
            }
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(new { objReg = objReg, objUDF = objUDF });

            return new MemoryStream(Encoding.UTF8.GetBytes(clsCommon.aaSerializeJsona(json)));
        }

        #endregion

        #region "----EditAddress----"
        public Stream EditAddress(Stream streamdata)
        {
            StreamReader reader = new StreamReader(streamdata);
            string res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();
            var @params = HttpUtility.ParseQueryString(res);
            dynamic rst = new ExpandoObject();
            dynamic billing = new ExpandoObject();
            dynamic shipping = new ExpandoObject();
            dynamic clientAddRefID = new ExpandoObject();
            string load_address = @params["load_address"];
            string userid = @params["userid"];
            string email = @params["userinfo[data][user_email]"];
            string h = "";

            if (load_address.ToLower().Trim().Equals("billing"))
            {
                rst = clsCustomers.getClientRefID(userid);
                if (rst != null)
                {
                    if (rst.rowID > 0)
                    {
                        clientAddRefID = clsCustomers.getClientAddRefID(rst.rowID, 0);
                        billing = EditBillingAddr(res, rst.rowID, clientAddRefID);
                    }
                }
            }
            else if (load_address.ToLower().Trim().Equals("shipping"))
            {
                rst = clsCustomers.getClientRefID(userid);
                if (rst != null)
                {
                    if (rst.rowID > 0)
                    {
                        clientAddRefID = clsCustomers.getClientAddRefID(rst.rowID, 1);
                        shipping = EditShippingAddr(res, rst.rowID, clientAddRefID);
                    }
                }
            }

            var DyObjectsList = new List<dynamic>();
            // string 
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                data = @params,
                clientRefID = rst,
                billing = billing,
                shipping = shipping,
                clientAddRefID = clientAddRefID
            });

            return new MemoryStream(Encoding.UTF8.GetBytes(clsCommon.aaSerializeJsona(json)));

        }

        #endregion

        public dynamic EditBillingAddr(string par, int ClientRefID, dynamic clientAddInfo)
        {
            dynamic rst = new ExpandoObject();
            var @params = HttpUtility.ParseQueryString(par);
            string billing_first_name = @params["billing_first_name"];
            string billing_last_name = @params["billing_last_name"];
            string billing_company = @params["billing_company"];
            string billing_email = @params["billing_email"];
            string billing_phone = @params["billing_phone"];
            string billing_country = @params["billing_country"];
            string billing_address_1 = @params["billing_address_1"];
            string billing_address_2 = @params["billing_address_2"];
            string billing_city = @params["billing_city"];
            string billing_state = @params["billing_state"];
            string billing_postcode = @params["billing_postcode"];
            string userid = @params["userid"];
            // int StateRefID = 0;
            rst.StateRefID = 0;
            dynamic ObjState = new ExpandoObject();
            dynamic findState = new ExpandoObject();
            
            findState = clsCustomers.findStateJson(billing_state);
            if (!findState.statename.Equals("0"))
            {
                ObjState = clsCustomers.getStateRefID(findState.statename);

                if (ObjState != null)
                {
                    if (ObjState.rowID > 0) { rst.StateRefID = ObjState.rowID; }
                }
            }
            else {

            }

            #region state old
            //if (mapState.ContainsKey(billing_state))
            //{
            //    //StateRefID = mapState[billing_state];
            //    if (!mapState[billing_state].Trim().Equals(""))
            //    {
            //        ObjState = clsCustomers.getStateRefID(mapState[billing_state]);

            //        if (ObjState != null)
            //        {
            //            if (ObjState.rowID > 0) { rst.StateRefID = ObjState.rowID; }
            //        }
            //    }
            //}
            #endregion

            string retval;
            int newID = 0;

            string queryString = "";
            string STS = "";
            using (SqlConnection connection = new SqlConnection(cons))
            {
                SqlCommand command;

                #region insert
                queryString = "INSERT INTO dbo.tbl_SetupClientAddress (ClientRefID , IsShip , ShipToName , ContactPerson , Telephone1 , Telephone2 ," +
                   "Fax , Address , PostCode , City , StateRefID ,   UpdBy , UpdOn)" +
                   " VALUES (@ClientRefID , @IsShip , @ShipToName , @ContactPerson , @Telephone1 , @Telephone2 , @Fax , @Address , @PostCode , @City , @StateRefID ,   @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";
                STS = "Insert";
                command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", ClientRefID);
                command.Parameters.AddWithValue("@IsShip", 0);
                command.Parameters.AddWithValue("@ShipToName", billing_first_name.Trim() + " , " + billing_last_name.Trim());
                command.Parameters.AddWithValue("@ContactPerson", "");
                command.Parameters.AddWithValue("@Telephone1", "");
                command.Parameters.AddWithValue("@Telephone2", "");
                command.Parameters.AddWithValue("@Fax", "");
                command.Parameters.AddWithValue("@Address", billing_address_1.Trim() + " , " + billing_address_2.Trim());
                command.Parameters.AddWithValue("@PostCode", billing_postcode.Trim());
                command.Parameters.AddWithValue("@City", billing_city.Trim());
                command.Parameters.AddWithValue("@StateRefID", rst.StateRefID);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Inserted ";

                }
                catch (SqlException e) { retval = e.Message; }
                #endregion
                #region edit
                //if (clientAddInfo.rowID > 0)
                //{
                //    queryString = "UPDATE dbo.tbl_SetupClientAddress SET ClientRefID = @ClientRefID , IsShip = @IsShip , ShipToName = @ShipToName , ContactPerson = @ContactPerson  , Telephone1 = @Telephone1, Telephone2 = @Telephone2 ," +
                //   "Fax =  @Fax , Address = @Address , PostCode = @PostCode , City = @City , StateRefID =  @StateRefID  ,   UpdBy =  @UpdBy , UpdOn = GETDATE() WHERE ClientAddRefID = @ClientAddRefID; SELECT SCOPE_IDENTITY();";
                //    STS = "Update";
                //    command = new SqlCommand(queryString, connection);
                //    command.Parameters.AddWithValue("@ClientAddRefID", clientAddInfo.rowID);
                //    command.Parameters.AddWithValue("@ClientRefID", ClientRefID);
                //    command.Parameters.AddWithValue("@IsShip", 0);
                //    command.Parameters.AddWithValue("@ShipToName", billing_first_name.Trim() + " , " + billing_last_name.Trim());
                //    command.Parameters.AddWithValue("@ContactPerson", "");
                //    command.Parameters.AddWithValue("@Telephone1", "");
                //    command.Parameters.AddWithValue("@Telephone2", "");
                //    command.Parameters.AddWithValue("@Fax", "");
                //    command.Parameters.AddWithValue("@Address", billing_address_1.Trim() + " , " + billing_address_2.Trim());
                //    command.Parameters.AddWithValue("@PostCode", billing_postcode.Trim());
                //    command.Parameters.AddWithValue("@City", billing_city.Trim());
                //    command.Parameters.AddWithValue("@StateRefID", rst.StateRefID);
                //    command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                //    connection.Open();
                //    try
                //    {
                //        newID = clientAddInfo.rowID;
                //        newID = command.ExecuteNonQuery();
                //        retval = "Updated Inserted ";

                //    }
                //    catch (SqlException e) { retval = e.Message; }
                //}
                //else
                //{
                //    queryString = "INSERT INTO dbo.tbl_SetupClientAddress (ClientRefID , IsShip , ShipToName , ContactPerson , Telephone1 , Telephone2 ," +
                //    "Fax , Address , PostCode , City , StateRefID ,   UpdBy , UpdOn)" +
                //    " VALUES (@ClientRefID , @IsShip , @ShipToName , @ContactPerson , @Telephone1 , @Telephone2 , @Fax , @Address , @PostCode , @City , @StateRefID ,   @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";
                //    STS = "Insert";
                //    command = new SqlCommand(queryString, connection);
                //    command.Parameters.AddWithValue("@ClientRefID", ClientRefID);
                //    command.Parameters.AddWithValue("@IsShip", 0);
                //    command.Parameters.AddWithValue("@ShipToName", billing_first_name.Trim() + " , " + billing_last_name.Trim());
                //    command.Parameters.AddWithValue("@ContactPerson", "");
                //    command.Parameters.AddWithValue("@Telephone1", "");
                //    command.Parameters.AddWithValue("@Telephone2", "");
                //    command.Parameters.AddWithValue("@Fax", "");
                //    command.Parameters.AddWithValue("@Address", billing_address_1.Trim() + " , " + billing_address_2.Trim());
                //    command.Parameters.AddWithValue("@PostCode", billing_postcode.Trim());
                //    command.Parameters.AddWithValue("@City", billing_city.Trim());
                //    command.Parameters.AddWithValue("@StateRefID", rst.StateRefID);
                //    command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                //    connection.Open();
                //    try
                //    {
                //        newID = Convert.ToInt32(command.ExecuteScalar());
                //        retval = "Successfully Inserted ";

                //    }
                //    catch (SqlException e) { retval = e.Message; }
                //}
                #endregion


                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.newID = newID;
                rst.retval = retval;
                rst.STS = STS;
                return rst;

            }

        }

        public dynamic EditShippingAddr(string par, int ClientRefID, dynamic clientAddInfo)
        {
            dynamic rst = new ExpandoObject();
            var @params = HttpUtility.ParseQueryString(par);
            string shipping_first_name = @params["shipping_first_name"];
            string shipping_last_name = @params["shipping_last_name"];
            string shipping_company = @params["shipping_company"];
            string shipping_country = @params["shipping_country"];
            string shipping_address_1 = @params["shipping_address_1"];
            string shipping_address_2 = @params["shipping_address_2"];
            string shipping_city = @params["shipping_city"];
            string shipping_state = @params["shipping_state"];
            string shipping_postcode = @params["shipping_postcode"];
            string userid = @params["userid"];

            rst.StateRefID = 0;
            dynamic ObjState = new ExpandoObject();

            dynamic findState = new ExpandoObject();

            findState = clsCustomers.findStateJson(shipping_state);
            if (!findState.statename.Equals("0"))
            {
                ObjState = clsCustomers.getStateRefID(findState.statename);
                if (ObjState != null)
                {
                    if (ObjState.rowID > 0) { rst.StateRefID = ObjState.rowID; }
                }
            }
            else
            {

            }

            #region old state
            //if (mapState.ContainsKey(shipping_state))
            //{
            //    if (!mapState[shipping_state].Trim().Equals(""))
            //    {
            //        ObjState = clsCustomers.getStateRefID(mapState[shipping_state]);
            //        if (ObjState != null)
            //        {
            //            if (ObjState.rowID > 0) { rst.StateRefID = ObjState.rowID; }
            //        }
            //    }
            //}
            #endregion

            string retval;
            int newID = 0;

            string queryString = "";
            string STS = "";
            using (SqlConnection connection = new SqlConnection(cons))
            {
                SqlCommand command;

                queryString = "INSERT INTO dbo.tbl_SetupClientAddress (ClientRefID , IsShip , ShipToName , ContactPerson , Telephone1 , Telephone2 ," +
                  "Fax , Address , PostCode , City , StateRefID ,   UpdBy , UpdOn)" +
                  " VALUES (@ClientRefID , @IsShip , @ShipToName , @ContactPerson , @Telephone1 , @Telephone2 , @Fax , @Address , @PostCode , @City , @StateRefID ,   @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";

                command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ClientRefID", ClientRefID);
                command.Parameters.AddWithValue("@IsShip", 1);
                command.Parameters.AddWithValue("@ShipToName", shipping_first_name + " , " + shipping_last_name);
                command.Parameters.AddWithValue("@ContactPerson", "");
                command.Parameters.AddWithValue("@Telephone1", "");
                command.Parameters.AddWithValue("@Telephone2", "");
                command.Parameters.AddWithValue("@Fax", "");
                command.Parameters.AddWithValue("@Address", shipping_address_1 + " , " + shipping_address_2);
                command.Parameters.AddWithValue("@PostCode", shipping_postcode.Trim());
                command.Parameters.AddWithValue("@City", shipping_city.Trim());
                command.Parameters.AddWithValue("@StateRefID", rst.StateRefID);
                command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                connection.Open();
                try
                {
                    newID = Convert.ToInt32(command.ExecuteScalar());
                    retval = "Successfully Inserted ";

                }
                catch (SqlException e) { retval = e.Message; }

                #region edit
                //if (clientAddInfo.rowID > 0)
                //{
                //    queryString = "UPDATE dbo.tbl_SetupClientAddress SET ClientRefID = @ClientRefID , IsShip = @IsShip , ShipToName = @ShipToName , ContactPerson = @ContactPerson  , Telephone1 = @Telephone1, Telephone2 = @Telephone2 ," +
                //   "Fax =  @Fax , Address = @Address , PostCode = @PostCode , City = @City , StateRefID =  @StateRefID  ,   UpdBy =  @UpdBy , UpdOn = GETDATE() WHERE ClientAddRefID = @ClientAddRefID; SELECT SCOPE_IDENTITY();";
                //    STS = "Update";
                //    command = new SqlCommand(queryString, connection);
                //    command.Parameters.AddWithValue("@ClientAddRefID", clientAddInfo.rowID);
                //    command.Parameters.AddWithValue("@ClientRefID", ClientRefID);
                //    command.Parameters.AddWithValue("@IsShip", 1);
                //    command.Parameters.AddWithValue("@ShipToName", shipping_first_name + " , " + shipping_last_name);
                //    command.Parameters.AddWithValue("@ContactPerson", "");
                //    command.Parameters.AddWithValue("@Telephone1", "");
                //    command.Parameters.AddWithValue("@Telephone2", "");
                //    command.Parameters.AddWithValue("@Fax", "");
                //    command.Parameters.AddWithValue("@Address", shipping_address_1 + " , " + shipping_address_2);
                //    command.Parameters.AddWithValue("@PostCode", shipping_postcode.Trim());
                //    command.Parameters.AddWithValue("@City", shipping_city.Trim());
                //    command.Parameters.AddWithValue("@StateRefID", rst.StateRefID);
                //    command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                //    connection.Open();
                //    try
                //    {
                //        newID = command.ExecuteNonQuery();
                //        retval = "Updated Inserted ";

                //    }
                //    catch (SqlException e) { retval = e.Message; }
                //}
                //else
                //{
                //    queryString = "INSERT INTO dbo.tbl_SetupClientAddress (ClientRefID , IsShip , ShipToName , ContactPerson , Telephone1 , Telephone2 ," +
                //   "Fax , Address , PostCode , City , StateRefID ,   UpdBy , UpdOn)" +
                //   " VALUES (@ClientRefID , @IsShip , @ShipToName , @ContactPerson , @Telephone1 , @Telephone2 , @Fax , @Address , @PostCode , @City , @StateRefID ,   @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";

                //    command = new SqlCommand(queryString, connection);
                //    command.Parameters.AddWithValue("@ClientRefID", ClientRefID);
                //    command.Parameters.AddWithValue("@IsShip", 1);
                //    command.Parameters.AddWithValue("@ShipToName", shipping_first_name + " , " + shipping_last_name);
                //    command.Parameters.AddWithValue("@ContactPerson", "");
                //    command.Parameters.AddWithValue("@Telephone1", "");
                //    command.Parameters.AddWithValue("@Telephone2", "");
                //    command.Parameters.AddWithValue("@Fax", "");
                //    command.Parameters.AddWithValue("@Address", shipping_address_1 + " , " + shipping_address_2);
                //    command.Parameters.AddWithValue("@PostCode", shipping_postcode.Trim());
                //    command.Parameters.AddWithValue("@City", shipping_city.Trim());
                //    command.Parameters.AddWithValue("@StateRefID", rst.StateRefID);
                //    command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                //    connection.Open();
                //    try
                //    {
                //        newID = Convert.ToInt32(command.ExecuteScalar());
                //        retval = "Successfully Inserted ";

                //    }
                //    catch (SqlException e) { retval = e.Message; }
                //}
                #endregion 
                #region test
                //SqlCommand command;
                //string queryString = "INSERT INTO dbo.tbl_SetupClientAddress (ClientRefID , IsShip , ShipToName , ContactPerson , Telephone1 , Telephone2 ," +
                //    "Fax , Address , PostCode , City , StateRefID ,   UpdBy , UpdOn)" +
                //    " VALUES (@ClientRefID , @IsShip , @ShipToName , @ContactPerson , @Telephone1 , @Telephone2 , @Fax , @Address , @PostCode , @City , @StateRefID ,   @UpdBy , GETDATE()); SELECT SCOPE_IDENTITY();";

                //command = new SqlCommand(queryString, connection);
                //command.Parameters.AddWithValue("@ClientRefID", ClientRefID);
                //command.Parameters.AddWithValue("@IsShip", 1);
                //command.Parameters.AddWithValue("@ShipToName", shipping_first_name+" , "+ shipping_last_name);
                //command.Parameters.AddWithValue("@ContactPerson", "");
                //command.Parameters.AddWithValue("@Telephone1", "");
                //command.Parameters.AddWithValue("@Telephone2", "");
                //command.Parameters.AddWithValue("@Fax", "");
                //command.Parameters.AddWithValue("@Address", shipping_address_1+" , "+ shipping_address_2);
                //command.Parameters.AddWithValue("@PostCode", shipping_postcode.Trim());
                //command.Parameters.AddWithValue("@City", shipping_city.Trim());
                //command.Parameters.AddWithValue("@StateRefID", rst.StateRefID);
                //command.Parameters.AddWithValue("@UpdBy", "PRADMIN");
                //connection.Open();
                //try
                //{
                //    newID = Convert.ToInt32(command.ExecuteScalar());
                //    retval = "Successfully Inserted ";

                //}
                //catch (SqlException e) { retval = e.Message; }
                #endregion
                if (connection.State == System.Data.ConnectionState.Open) connection.Close();
                rst.newID = newID;
                rst.retval = retval;
                return rst;

            }

        }

        public Stream EditCustomer(Stream streamdata)
        {
            StreamReader reader = new StreamReader(streamdata);
            string res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();
            var @params = HttpUtility.ParseQueryString(res);
            dynamic DyObj = new ExpandoObject();
            DyObj.userid = @params["ID"];
            DyObj.first_name = @params["first_name"];
            DyObj.last_name = @params["last_name"];
            DyObj.user_email = @params["user_email"];
            DyObj.role = "EC";
            DyObj.name = DyObj.first_name + " , " + DyObj.last_name;

            var DyObjectsList = new List<dynamic>();
            dynamic objReg = new ExpandoObject();
            dynamic objCheckID = new ExpandoObject();
            dynamic objUDF = new ExpandoObject();
            dynamic obUpdateCust = new ExpandoObject();

            // check client id
            if (DyObj.userid.Trim().Equals("0"))
            {
                // check use email
                objCheckID = clsCustomers.getClientRefID(DyObj.user_email.Trim());
                objCheckID.ERegistered = "No";
            }
            else
            {
                // check userid
                objCheckID = clsCustomers.getClientRefID(DyObj.userid.Trim());
                objCheckID.ERegistered = "Yes";

                //if not exist
                if (objCheckID.rowID == 0)
                {
                    // register as new customer
                    objCheckID.sts = "will reg";
                    objReg = clsCustomers.RegisterNewCustomer(DyObj);
                    if (objReg != null)
                    {
                        objReg.newID = Convert.ToInt32(objReg.newID);
                        if (objReg.newID > 0)
                        {
                            objUDF = clsCustomers.InsertClientUDF(objReg.newID);
                        }
                    }

                }
                else
                {
                    // update info
                    objCheckID.sts = "will update";
                    obUpdateCust = clsCustomers.EditCustomer(DyObj);

                }
            }


            string json = Newtonsoft.Json.JsonConvert.SerializeObject(
                new
                {
                    objReg = objReg,
                    objCheckID = objCheckID,
                    obUpdateCust = obUpdateCust
                });

            return new MemoryStream(Encoding.UTF8.GetBytes(clsCommon.aaSerializeJsona(json)));
        }


        //Dictionary<string, string> mapState = new Dictionary<string, string> {
        //    { "JHR", "Johor" },
        //    { "KDH", "Kedah" },
        //    { "KTN", "Kelantan" },
        //    { "LBN", "Labuan" },
        //    { "MLK", "Melaka" },
        //    { "NSN", "Negeri Sembilan" },
        //    { "PHG", "Pahang" },
        //    { "PNG", "Penang" },
        //    { "PRK", "Perak" },
        //    { "PLS", "Perlis" },
        //    { "SBH", "Sabah" },
        //    { "SWK", "Sarawak" },
        //    { "SGR", "Selangor" },
        //    { "TRG", "Terengganu" },
        //    { "PJY", "Putrajaya" },
        //    { "KUL", "Kuala Lumpur" }
        //};


    }
}
