﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.ServiceModel.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace vtm.Common
{
    public class clsCommon
    {
        #region "----Variables----"
        private byte[] key;
        private byte[] initVector;
        private string strMessage;
        private byte[] InputInBytes;
        //public static string sWebServerName = ConfigurationManager.AppSettings["WebServerName"].ToString();
        //public static string sServerName = ConfigurationManager.AppSettings["ServerName"].ToString();
        //public static string sDatabaseName = ConfigurationManager.AppSettings["DatabaseName"].ToString();
        //public static string sDBUsrID = ConfigurationManager.AppSettings["DatabaseUsrID"].ToString();
        //public static string sDBPwd = ConfigurationManager.AppSettings["DatabasePwd"].ToString();
        ////     MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString());
        //    DBConnector dbconn = new DBConnector(sServerName, sDatabaseName, sDBUsrID, sDBPwd, ref blnDBConnStatus, ref sDBMsg);

     //   string strConnection = ConfigurationManager.ConnectionStrings["connectionString"].ToString();

        public static string sDBMsg = "";
        public static bool blnDBConnStatus = false;
        public static string myIP = GetIPAddress();
        public static int Status;
        public static string Flag = "0";
        public static string Msg = "";
        public static string Action = "";
        public string OutputDateFormat = "dd/MM/yyyy hh:mm:ss tt";
        public string InputDateFormat = "yyyy-MM-dd HH:mm:ss tt";
        public string db_Name = "opss_v2";

        string strMsg;

        public string ClassMessage
        {
            get
            {
                return this.strMessage;
            }
        }
        #endregion

        //#region
        //public string getConnection() {
        //    return strConnection;            
        //}
        //#endregion

        #region "----Get IP----"
        public static string GetIPAddress()
        {
            string IPAdd = null;
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                var addr = ni.GetIPProperties().GatewayAddresses.FirstOrDefault();
                if (addr != null)
                {
                    if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                    {
                        Console.WriteLine(ni.Name);
                        foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                        {
                            if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                                IPAdd = ip.Address.ToString();
                            }
                        }
                    }
                }
            }
            return IPAdd;
        }
        #endregion

        #region "---Encryption----"
        public string Encrypt(string TextToEncrypt)
        {
            string str = "";
            try
            {
                byte[] bytes = new UTF8Encoding().GetBytes(TextToEncrypt);
                ICryptoTransform encryptor = new TripleDESCryptoServiceProvider().CreateEncryptor(this.key, this.initVector);
                MemoryStream memoryStream = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write);
                cryptoStream.Write(bytes, 0, bytes.Length);
                cryptoStream.FlushFinalBlock();
                memoryStream.Position = 0L;
                byte[] numArray = new byte[checked((int)(memoryStream.Length - 1L) + 1)];
                memoryStream.Read(numArray, 0, checked((int)memoryStream.Length));
                cryptoStream.Close();
                str = Convert.ToBase64String(numArray);
            }
            catch (Exception ex)
            {
                //ProjectData.SetProjectError(ex);
                this.strMessage = ex.Message;
                //ProjectData.ClearProjectError();
            }
            return str;
        }
        #endregion

        #region "---Decryption----"
        public string Decrypt(string InputInText)
        {
            string @string = "";
            try
            {
                this.InputInBytes = Convert.FromBase64String(InputInText);
                UTF8Encoding utF8Encoding = new UTF8Encoding();
                ICryptoTransform decryptor = new TripleDESCryptoServiceProvider().CreateDecryptor(this.key, this.initVector);
                MemoryStream memoryStream = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Write);
                cryptoStream.Write(this.InputInBytes, 0, this.InputInBytes.Length);
                cryptoStream.FlushFinalBlock();
                memoryStream.Position = 0L;
                byte[] numArray = new byte[checked((int)(memoryStream.Length - 1L) + 1)];
                memoryStream.Read(numArray, 0, checked((int)memoryStream.Length));
                cryptoStream.Close();
                @string = new UTF8Encoding().GetString(numArray);
            }
            catch (Exception ex)
            {
                //ProjectData.SetProjectError(ex);
                this.strMessage = ex.Message;
                //ProjectData.ClearProjectError();
            }
            return @string;
        }
        #endregion

   
        public string SerializeJson(object obj)
        {
            var s = new JavaScriptSerializer();
            string jsonClient = s.Serialize(obj);

            WebOperationContext.Current.OutgoingResponse.ContentType =
                "application/json; charset=utf-8";

            return System.Text.RegularExpressions.Regex.Unescape(jsonClient);
        }

        public string aaSerializeJsona(String obj)
        {
            var s = new JavaScriptSerializer();
            string jsonClient = s.Serialize(obj);

            WebOperationContext.Current.OutgoingResponse.ContentType =
                "application/json; charset=utf-8";

            return System.Text.RegularExpressions.Regex.Unescape(obj);
        }

        public clsCommon()
        {
            this.key = new byte[24] { (byte)10, (byte)20, (byte)65, (byte)77, (byte)23, (byte)86, (byte)45, (byte)32, (byte)33, (byte)101, (byte)211, (byte)74, (byte)59, (byte)14, (byte)15, (byte)26, (byte)9, (byte)91, (byte)41, (byte)20, (byte)21, (byte)153, (byte)195, (byte)24 };
            this.initVector = new byte[8] { (byte)77, unchecked((byte)sbyte.MinValue), (byte)90, (byte)55, (byte)42, (byte)153, (byte)27, (byte)39 };
        }

        #region "---FormatDateToDBDate----"
        public string FormatDateToDBDate(string strDate, string strDelimiter)
        {

            string strDbDt;
            string[] strArray;
            DateTime dtNewDate;

            if (strDate != "")
            {

                // Start Splitting Date & Time
                // End Splitting Date & Time
                strArray = Regex.Split(strDate, " ");

                // Start Splitting Day, Month, Year
                // End Splitting Day, Month, Year

                strArray = Regex.Split(strArray[0].ToString().Trim(), strDelimiter);

                // Start Format Date
                // strDbDt = strArray(1) & "/" & strArray(0) & "/" & strArray(2)

                {
                    dtNewDate = new DateTime(Convert.ToInt32(strArray[2]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[0]), 0, 0, 0);
                    strDbDt = Convert.ToString(dtNewDate);
                }
                // End Format Date
            }

            else
            {
                strDbDt = strDate;

            }

            return strDbDt;
        }
        #endregion

        #region "---Send Mail---"
        public int SendMail(string mailfrom, string mailto, string mailsubject, string mailbody, string isHtml, ref string strMsg)
        {
            int status = 0;

            try
            {
                // Command line argument must the the SMTP host.
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = "smtp.gmail.com";
                //client.Host = "mail.powiis.edu.my";
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("pr.prsystem@gmail.com", "prprsystem");
                //client.Credentials = new System.Net.NetworkCredential("pradmin", "prp@ssw0rd");

                MailMessage mm = new MailMessage();
                mm.To.Add(mailto);
                //mm.CC.Add("pradmin@powiis.edu.my");
                mm.From = new System.Net.Mail.MailAddress("pradmin@powiis.edu.my");
                mm.Subject = mailsubject;
                mm.Body = mailbody;

                //MailMessage mm = new MailMessage(
                //    mailfrom,
                //    mailto,
                //    mailsubject,
                //    mailbody
                //);
                if (isHtml == "Yes")
                {
                    mm.IsBodyHtml = true;
                }
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);
                mm = null;
                status = 1;
            }
            catch (Exception ex)
            {
                status = 0;
                strMsg = ex.Message;
            }

            return status;
        }
        #endregion

    

    
      

      

   



    

        #region "----Get Column Name----"
        public string[] GetColName(DataSet dsResult)
        {
            string[] Array = new string[0];

            if (dsResult != null)
            {
                Array = new string[dsResult.Tables[0].Columns.Count];

                for (int i = 0; i < dsResult.Tables[0].Columns.Count; i++)
                {
                    Array[i] = dsResult.Tables[0].Columns[i].ToString();
                }
            }
            return Array;
        }
        #endregion

        #region "----Validation----"
        public void Validation(bool success, ref int sts)
        {
            if (success == true)
            {

                sts = 1;

            }
            else
            {
                sts = 0;

            }
        }
        #endregion

        #region "----Convert DateFormat----"
        public string ConvertDateFormat(string stringDate)
        {

            stringDate = Convert.ToDateTime(stringDate).ToString(InputDateFormat);

            return stringDate;
        }
        #endregion

        #region "----Date Validation----"
        public bool DateValidation(string[] ArrayColName, int No)
        {
            bool @bool = true;
            string[] arrayCol = configureDateCol();

            for (int ii = 0; ii < arrayCol.Length; ii++)
            {

                if (arrayCol[ii] == ArrayColName[No])
                {
                    @bool = false;
                    break;

                }
            }

            return @bool;
        }
        #endregion

        #region "----Convert string to datetime and datetime to string format----"
        // 
        public string ConvertDate(string stringDate)
        {
            DateTime DateTime = DateTime.ParseExact(stringDate, OutputDateFormat, CultureInfo.InvariantCulture);
            string outputDate = Convert.ToDateTime(DateTime).ToString(OutputDateFormat);

            return outputDate;
        }
        #endregion

        #region "----Set Datatable Column Name----"
        public string[] configureDateCol()
        {

            string[] arrayCol = { "CreatedDate", "TimeStamp", "LogUTCTimeStamp", "UpdatedDate" };

            return arrayCol;
        }
        #endregion

        public List<dynamic> setColList(string[] arrayCol)
        {

            var colList = new List<dynamic>();
            dynamic obj = new ExpandoObject();

            for (int i = 0; i < arrayCol.Length; i++)
            {
                obj = new ExpandoObject();
                var objList = obj as IDictionary<string, object>;

                objList["mData"] = arrayCol[i];
                colList.Add(obj);
            }
            //colList.Add(obj);

            return colList;
        }
    }
}